package qiushibaike

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
)

func UnmarshalJson(input []byte, result interface{}) (map[string]interface{}, error) {
	// unmarshal json to a map
	foomap := make(map[string]interface{})
	json.Unmarshal(input, &foomap)

	// create a mapstructure decoder
	var md mapstructure.Metadata
	decoder, err := mapstructure.NewDecoder(
		&mapstructure.DecoderConfig{
			Metadata: &md,
			Result:   result,
		})
	if err != nil {
		return nil, err
	}

	// decode the unmarshalled map into the given struct
	if err := decoder.Decode(foomap); err != nil {
		return nil, err
	}

	// copy and return unused fields
	//unused := map[string]interface{}{}
	//for _, k := range md.Unused {
	//	unused[k] = foomap[k]
	//}
	//return unused, nil
	return foomap, nil
}
