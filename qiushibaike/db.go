package qiushibaike

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type ArticlePageHandler struct {
	Collection *mongo.Collection
	ArticleID  int
	ModelID    interface{}
}

type ArticleDBModel struct {
	//ID int `json:"_id"`
	ArticleID int `json:"article_id"`

	Data map[string]interface{} `json:"data"`

	Finished   bool      `json:"finished"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}

func NewArticlePageHandler(collection *mongo.Collection, articleID int) *ArticlePageHandler {
	return &ArticlePageHandler{
		Collection: collection,
		ArticleID:  articleID,
		ModelID:    nil,
	}
}

func InitMongoCollection(client *mongo.Client, dbName string, collectionName string) (*mongo.Collection, error) {
	collection := client.Database(dbName).Collection(collectionName)

	_, err := collection.Indexes().CreateOne(context.TODO(), mongo.IndexModel{
		Keys: bson.M{"article_id": 1},
	})

	if err != nil {
		return nil, err
	}
	return collection, nil
}

func (h *ArticlePageHandler) StoreArticlePage(jsonBody *ArticleInfoStruct, rawBody map[string]interface{}, isFirstPage bool) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if isFirstPage {
		model := ArticleDBModel{
			ArticleID:  h.ArticleID,
			Data:       rawBody,
			Finished:   false,
			CreateTime: time.Now(),
			UpdateTime: time.Now(),
		}
		res, err := h.Collection.InsertOne(ctx, model)
		if err != nil {
			return err
		}
		h.ModelID = res.InsertedID
	} else {
		_, err := h.Collection.UpdateByID(ctx, h.ModelID, &bson.M{
			"$push": &bson.M{
				"data.comments": &bson.M{
					"$each": jsonBody.Comments,
				},
			},
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *ArticlePageHandler) Finish() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := h.Collection.UpdateByID(ctx, h.ModelID, bson.M{
		"$set": bson.M{
			"finished":    true,
			"update_time": time.Now(),
		},
	})
	return err
}
