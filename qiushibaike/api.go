package qiushibaike

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"time"
)

const PAGE_COUNT = 200

type ArticleInfoStruct struct {
	HasMore   bool   `json:"has_more"`
	Err       int    `json:"err"`
	ErrMsg    string `json:"err_msg"`
	Total     int    `json:"total"`
	TotalUser int    `json:"total_user"`
	//Comments  []struct {
	//	Status        int           `json:"status"`
	//	Liked         bool          `json:"liked"`
	//	ChildrenCount int           `json:"children_count"`
	//	UserId        int           `json:"user_id"`
	//	OriginId      int           `json:"origin_id"`
	//	IsMe          bool          `json:"is_me"`
	//	CreatedAt     int           `json:"created_at"`
	//	CommentId     int           `json:"comment_id"`
	//	Children      []interface{} `json:"children"`
	//	Content       string        `json:"content"`
	//	LikeCount     int           `json:"like_count"`
	//	User          struct {
	//		Medium     string `json:"medium"`
	//		Thumb      string `json:"thumb"`
	//		Gender     string `json:"gender"`
	//		Age        int    `json:"age"`
	//		NickStatus int    `json:"nick_status"`
	//		State      string `json:"state"`
	//		Role       string `json:"role"`
	//		Astrology  string `json:"astrology"`
	//		Login      string `json:"login"`
	//		Icon       string `json:"icon"`
	//		Id         int    `json:"id"`
	//		Uid        int    `json:"uid"`
	//	} `json:"user"`
	//	AtUsers    []interface{} `json:"at_users"`
	//	IpLocation string        `json:"ip_location"`
	//	ArticleId  int           `json:"article_id"`
	//	Id         int           `json:"id"`
	//} `json:"comments"`
	Comments []map[string]interface{} `json:"comments"`
}

var Client *fasthttp.Client
var CommonHeader *fasthttp.RequestHeader

func init() {
	readTimeout, _ := time.ParseDuration("4000ms")
	writeTimeout, _ := time.ParseDuration("4000ms")
	maxIdleConnDuration, _ := time.ParseDuration("1h")

	Client = &fasthttp.Client{
		//Addr:                          "circle.qiushibaike.com:443", // The host address and port must be set explicitly
		ReadTimeout:                   readTimeout,
		WriteTimeout:                  writeTimeout,
		MaxIdleConnDuration:           maxIdleConnDuration,
		NoDefaultUserAgentHeader:      true, // Don't send: User-Agent: fasthttp
		DisableHeaderNamesNormalizing: true, // If you set the case on your headers correctly you can enable this
		DisablePathNormalizing:        true,
		// increase DNS cache time to an hour instead of default minute
		Dial: (&fasthttp.TCPDialer{
			Concurrency:      4096,
			DNSCacheDuration: time.Hour,
		}).Dial,
	}

	CommonHeader = &fasthttp.RequestHeader{}
	CommonHeader.Add("Source", "ios_11.25.3")
	CommonHeader.Add("app", "1")
	CommonHeader.Add("Uuid", "ios_f92e58b77ecc467e972927fdd8179356")
	CommonHeader.Add("User-Agent", "QiuBai/11.25.3 rv:11.25.3.88 (iPhone; iOS 15.4.1; zh_CN) PLHttpClient/1_WIFI")
}

func InitHttp() {
}

func GetArticleInfoRaw(articleID int, commentPage int) (rawJson map[string]interface{}, jsonBody *ArticleInfoStruct, err error) {
	queryUrl := fmt.Sprintf("https://circle.qiushibaike.com:443/v2/article/%d/info?count=%d&page=%d", articleID, PAGE_COUNT, commentPage)

	url := fasthttp.AcquireURI()
	err = url.Parse(nil, []byte(queryUrl))

	req := fasthttp.AcquireRequest()
	CommonHeader.CopyTo(&req.Header)
	req.Header.SetMethod(fasthttp.MethodGet)

	req.SetURI(url)          // copy url into request
	fasthttp.ReleaseURI(url) // now you may release the URI

	resp := fasthttp.AcquireResponse()

	err = Client.Do(req, resp)

	//fmt.Printf(req.String())
	fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)

	rawJson = nil;
	jsonBody = nil
	if err == nil {
		rawBody := resp.Body()
		if resp.StatusCode() != 200 {
			err = fmt.Errorf("server returned %d: %s", resp.StatusCode(), string(rawBody))
		}

		if rawJson, err = UnmarshalJson(rawBody, &jsonBody); err != nil {
			rawJson = nil;
			jsonBody = nil
			return
		}
		return
	} else {
		return
	}
}

func GetArticleInfo(articleID int, commentPage int) (rawBody map[string]interface{}, jsonBody *ArticleInfoStruct, err error) {
	rawBody = nil

	rawBody, jsonBody, err = GetArticleInfoRaw(articleID, commentPage)
	if err != nil {
		return
	}

	//if jsonbody.Err != 0 {
	//	err = fmt.Errorf("server interface error: %d, %s", jsonbody.Err, string(rawBody))
	//	return
	//}

	return
}
