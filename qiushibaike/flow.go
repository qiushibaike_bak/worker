package qiushibaike

import (
	"fmt"
	log "github.com/sirupsen/logrus"
)

func RetrivePageFull(articleID int,
	onPageCallback func(jsonData *ArticleInfoStruct, rawData map[string]interface{}, isFirstPage bool) error,
) (firstPage *ArticleInfoStruct, retrivedCount int, err error) {

	log := log.WithField("article", articleID)

	firstPage = nil
	retrivedCount = 0

	page := 1
	retries := 0
	for {
		retries += 1
		log := log.WithField("page", page)
		log.Debugf("Query page %d!", page)
		rawBody, jsonBody, _err := GetArticleInfo(articleID, page)
		err = _err
		if err != nil {
			return
		}
		if jsonBody.Err != 0 {
			_jsonBody := jsonBody
			jsonBody = nil
			if _jsonBody.Err == 30001 {
				// 帖子不存在
				err = fmt.Errorf("Article not exist (%d): %s", _jsonBody.Err, rawBody)
				return
			}
			if retries > 4 {
				err = fmt.Errorf("Unknown server error %d: %s", _jsonBody.Err, rawBody)
				return
			}
			continue
		}
		isFirstPage := false
		if firstPage == nil {
			isFirstPage = true
			firstPage = jsonBody
		}

		log.Debugf("Page %d: %d/%d", page, len(jsonBody.Comments), jsonBody.Total)
		retrivedCount += len(jsonBody.Comments)
		log.Tracef("Page content: %s", rawBody)
		err = onPageCallback(jsonBody, rawBody, isFirstPage)
		if err != nil {
			err = fmt.Errorf("page handler error: %v", err)
		}

		retries = 0

		if !jsonBody.HasMore {
			break
		}
		page += 1
	}
	//log.Infof("Article finished! Total comments: %d", retrivedCount)
	return
}
