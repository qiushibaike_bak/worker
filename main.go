package main

import (
	"context"
	"fmt"
	"github.com/NyaMisty/qiushibaike_backup_worker/qiushibaike"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/writer"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"io/ioutil"
	"os"
	"strconv"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"go.uber.org/ratelimit"
)

func init() {
	qiushibaike.InitHttp()
}

var mongoClient *mongo.Client

func initMongo(connStr string) {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	mongoClient, err = mongo.Connect(ctx, options.Client().ApplyURI(connStr))
	if err != nil {
		panic(fmt.Sprintf("mongo connect fail: %s", err))
	}
	if err = mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		panic(fmt.Sprintf("mongo ping fail: %s", err))
	}

}

func initLog() {
	stdFormatter := &log.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02.15:04:05.000000",
		ForceColors:     true,
		DisableColors:   false,
	}
	fileFormatter := &log.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02.15:04:05.000000",
		ForceColors:     false,
		DisableColors:   true,
	}
	log.SetFormatter(stdFormatter)
	log.SetLevel(log.DebugLevel)

	logPath, _ := os.Getwd() // 后期可以配置
	logFile := logPath + "/qiushibaike_worker.log"
	errlogFile := logPath + "/qiushibaike_worker_err.log"
	lfHook := lfshook.NewHook(lfshook.PathMap{
		log.PanicLevel: logFile,
		log.FatalLevel: logFile,
		log.ErrorLevel: logFile,
		log.WarnLevel:  logFile,
		log.InfoLevel:  logFile,
		log.DebugLevel: logFile,
	}, fileFormatter)
	lferrHook := lfshook.NewHook(lfshook.PathMap{
		log.PanicLevel: errlogFile,
		log.FatalLevel: errlogFile,
		log.ErrorLevel: errlogFile,
		log.WarnLevel:  errlogFile,
	}, fileFormatter)

	log.SetOutput(ioutil.Discard)
	log.AddHook(&writer.Hook{
		Writer: os.Stderr,
		LogLevels: []log.Level{
			log.PanicLevel,
			log.FatalLevel,
			log.ErrorLevel,
			log.WarnLevel,
			log.InfoLevel,
		},
	})
	log.AddHook(lferrHook)
	log.AddHook(lfHook)
}

func main() {
	MONGO_ADDR := os.Args[1]                  // "mongodb://127.0.0.1:27017"
	WORKER_NUM, _ := strconv.Atoi(os.Args[2]) // 10
	RATE_LIMIT, _ := strconv.Atoi(os.Args[3]) // 10

	initLog()
	initMongo(MONGO_ADDR)
	collection, err := qiushibaike.InitMongoCollection(mongoClient, "qiushibaike_bak", "articles")
	if err != nil {
		panic(err)
	}

	res := collection.FindOne(
		context.TODO(),
		bson.M{},
		options.FindOne().SetSort(bson.M{"article_id": -1}),
	)
	maxArticle := qiushibaike.ArticleDBModel{}
	articleIDMax := 24667000
	if err := res.Decode(&maxArticle); err == nil {
		articleIDMax = maxArticle.ArticleID + 1
	}

	rl := ratelimit.New(RATE_LIMIT) // per second

	wg := sync.WaitGroup{}
	wg.Add(WORKER_NUM)
	processQueue := make(chan int, WORKER_NUM*5)

	for i := 0; i < WORKER_NUM; i++ {
		go func() {
			for {
				_ = rl.Take()
				articleId := <-processQueue
				if articleId == 0 {
					log.Infof("Thread exiting...")
					wg.Done()
					break
				}
				h := qiushibaike.NewArticlePageHandler(collection, articleId)
				page, retrivedCount, err := qiushibaike.RetrivePageFull(articleId, h.StoreArticlePage)
				pageTotal := 0
				if page != nil {
					pageTotal = page.Total
				}
				if err != nil {
					log.Infof("FAIL: article %d, total %d, retrived %d, err %v", h.ArticleID, pageTotal, retrivedCount, err)
				} else {
					log.Infof("SUCC: article %d, total %d, retrived %d", h.ArticleID, pageTotal, retrivedCount)
					err = h.Finish()
					if err != nil {
						log.Warnf("Failed to finish article %d, err %v", h.ArticleID, err)
					}
				}
			}
		}()
	}

	for articleId := articleIDMax; articleId > 0; articleId-- {
		processQueue <- articleId
	}

	wg.Wait()

	//ARTICLE := 24665137

	//log.Infof("Start!")

	//log.Infof("Retrived article %d, total %d, retrived %d, err %v", ARTICLE, page.Total, retrivedCount, err)
}
